; bogomips.asm
; 2016 Tormod Volden
;
; Detect vsync IRQ frequency (50Hz/PAL or 60Hz/NTSC) and
; CPU freq (0.9 or 1.8 MHz) by counting loop cycles between
; two vsync interrupts (Dragon and CoCo)
;
; To build:
; 	lwasm -b -o bogomips.bin bogomips.asm
;
; Results (XRoar):
;	pal  0.9	2216 "B"
;	pal  1.8	4439 "K"
;	ntsc 0.9	1859 "A"
;	ntsc 1.8	3726 "H"
;
; Close estimate (counting loop is 8 cycles):
;	pal  0.9	14218000/16/50/8 = 2221
;	pal  1.8	14218000/ 8/50/8 = 4443
;	ntsc 0.9	14318180/16/60/8 = 1864
;	ntsc 1.8	14318180/ 8/60/8 = 3728
;

irqv	equ $010C

	org $0C00

irqvb	rmb 2
result	equ irqvb

start
	ldx #0		; reset counter

* install IRQ handler
	ldu irqv+1	; backup orig handler
	stu irqvb
	ldu #irqh1
	stu irqv+1	; first handler enabled

* wait for start irq
wait 	bra wait

* counting loop (8 CPU cycles)
count
	leax 1,x
	bra count

* irq stopped loop
stopped
	stx result
	tfr x,d
	; jmp $957A	; print counter ($BDCC on CoCo Extended Basic)
	adda #'A'-7	; print one-letter result
	jmp [$A002]

* first handler starts counting
irqh1
	lda $FF02	; clear PIA interrupt flag for CB1/CB2
	ldx #count
	stx 10,s	; return address on stack
	ldx #irqh2	; set up second handler
	stx irqv+1
	rti
* second handler stops counting
irqh2
	lda $FF02	; clear PIA interrupt flag for CB1/CB2
	ldx #stopped
	stx 10,s	; return address on stack
	ldx irqvb	; restore orig handler
	stx irqv+1
	rti

	end start
